namespace :sms_config do
  desc "Update SMS available"
  task update_sms_available: :environment do
    sms_config = SMSConfig.new
    sms_config.update_sms_available(sms_config.get_sms_available + 1000)
    puts sms_config.get_sms_available
  end

  desc "Get SMS available"
  task get_sms_available: :environment do
    sms_config = SMSConfig.new
    puts sms_config.get_sms_available
  end
end
