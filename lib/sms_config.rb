class SMSConfig
  def initialize; end

  def update_sms_available(amount)
    Setting["read_only.sms_available"] = amount
  end

  def get_sms_available
    Setting["read_only.sms_available"]&.to_i || 0
  end

  def decrease_sms_available(amount)
    Setting["read_only.sms_available"] = get_sms_available - amount
  end

  def increase_sms_available(amount)
    Setting["read_only.sms_available"] = get_sms_available + amount
  end
end
