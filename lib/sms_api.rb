require "open-uri"
require "sms_config"

class SMSApi
  attr_accessor :client

  def initialize
    if url.present?
      @client = Net::HTTP.new(url.host, url.port)
      @client.use_ssl = true
      @sms_config = SMSConfig.new
    end
  end

  def url
    return "" unless end_point_available?
    URI.parse( Rails.application.secrets.sms_end_point )
  end

  def sms_deliver(phone, code)
    return stubbed_response unless end_point_available?
    return nil if @sms_config.get_sms_available <= 0

    headers = { "Content-Type" => "application/x-www-form-urlencoded" }
    data = "xml=#{get_xml_request(phone, code)}"
    response = nil

    begin
      response = @client.post(url.path, data, headers)
      @sms_config.decrease_sms_available(1)
    rescue => e
      response = nil
    end

    success?(response)
  end

  def success?(response)
    return false if response.blank?

    body = Hash.from_xml( response.body )
    body["result"]["status"] == "100"
  end

  def end_point_available?
    Rails.env.production?
  end

  def stubbed_response
    { "result" => { "action" => "sendsms", "status"=>"100", "msg"=>"Success" } }
  end

  private
    def get_xml_request(phone, code)
      URI.escape "<?xml version='1.0' encoding='ISO-8859-1'?>
      <!DOCTYPE sms SYSTEM 'sms.dtd'>
      <sms>
        <user>#{Rails.application.secrets.sms_username}</user>
        <password>#{Rails.application.secrets.sms_password}</password>
        <dst>
          <num>#{phone}</num>
        </dst>
        <txt>Clave para verificarte: #{code}. #{Setting['org_name']}.</txt>
      </sms>"
    end
end
