if SiteCustomization::Page.find_by_slug("census_terms").nil?
  page = SiteCustomization::Page.new(slug: "census_terms", status: "published")
  page.title = I18n.t("pages.census_terms.title")
  page.content = "<p>#{I18n.t("pages.census_terms.description")}</p>"
  page.save!
end
