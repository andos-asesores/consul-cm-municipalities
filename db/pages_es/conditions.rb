if SiteCustomization::Page.find_by_slug("conditions").present?
  page = SiteCustomization::Page.find_by_slug("conditions")
  page.title_es = I18n.t("pages.conditions.title", locale: :es)
  page.subtitle_es = I18n.t("pages.conditions.subtitle", locale: :es)
  page.content_es = "<p>#{I18n.t("pages.conditions.description", locale: :es)}</p>"
  page.save!
end
