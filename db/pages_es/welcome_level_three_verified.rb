if SiteCustomization::Page.find_by_slug("welcome_level_three_verified").present?
  page = SiteCustomization::Page.find_by_slug("welcome_level_three_verified")
  page.title_es = I18n.t("welcome.welcome.title", locale: :es)

  page.content_es = "<p>#{I18n.t("welcome.welcome.user_permission_info", locale: :es)}</p>
                  <ul>
                    <li>#{I18n.t("welcome.welcome.user_permission_debates", locale: :es)}</li>
                    <li>#{I18n.t("welcome.welcome.user_permission_proposal", locale: :es)}</li>
                    <li>#{I18n.t("welcome.welcome.user_permission_support_proposal", locale: :es)}</li>
                    <li>#{I18n.t("welcome.welcome.user_permission_votes", locale: :es)}</li>
                  </ul>

                  <p>#{I18n.t("welcome.welcome.user_permission_verify_info", locale: :es)}</p>

                  <p>#{I18n.t("account.show.verified_account", locale: :es)}</p>

                  <p><a href='/'>#{I18n.t("welcome.welcome.start_using_consul", locale: :es)}</a></p>"
  page.save!
end
