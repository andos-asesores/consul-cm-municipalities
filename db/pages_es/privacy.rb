if SiteCustomization::Page.find_by_slug("privacy").present?
  page = SiteCustomization::Page.find_by_slug("privacy")
  page.title_es = I18n.t("pages.privacy.title", locale: :es)
  page.content_es = I18n.t("pages.privacy.subtitle", locale: :es)
  page.save!
end
