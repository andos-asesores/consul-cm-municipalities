if SiteCustomization::Page.find_by_slug("faq").present?
  page = SiteCustomization::Page.find_by_slug("faq")
  page.title_es = I18n.t("pages.help.faq.page.title", locale: :es)
  page.content_es = "#{I18n.t("pages.help.faq.page.description", email: Setting["mailer_from_address"], locale: :es)}"
  page.save!
end
