if SiteCustomization::Page.find_by_slug("accessibility").present?
  page = SiteCustomization::Page.find_by_slug("accessibility")
  page.title_es = I18n.t("pages.accessibility.title", locale: :es)

  content = ""
  I18n.t("pages.accessibility.description", locale: :es).each_line do |line|
    content << "<p>#{line}</p>"
  end
  content << "<ul>"
  I18n.t("pages.accessibility.examples", locale: :es).each do |example|
    content << "<li>#{example}</li>"
  end
  content << "</ul>
              <h2>#{I18n.t("pages.accessibility.keyboard_shortcuts.title", locale: :es)}</h2>
              <p>#{I18n.t("pages.accessibility.keyboard_shortcuts.navigation_table.description", locale: :es)}</p>
              <table>
                <caption class='show-for-sr'>
                  #{I18n.t("pages.accessibility.keyboard_shortcuts.navigation_table.caption", locale: :es)}
                </caption>
                <thead>
                  <tr>
                    <th scope='col' class='text-center'>
                      #{I18n.t("pages.accessibility.keyboard_shortcuts.navigation_table.key_header", locale: :es)}
                    </th>
                    <th scope='col'>
                      #{I18n.t("pages.accessibility.keyboard_shortcuts.navigation_table.page_header", locale: :es)}
                    </th>
                  </tr>
                </thead>
                <tbody>"
  I18n.t("pages.accessibility.keyboard_shortcuts.navigation_table.rows", locale: :es).each do |row|
    content << "  <tr>
                    <td class='text-center'>#{row[:key_column]}</td>
                    <td>#{row[:page_column]}</td>
                  </tr>"
  end
  content << "  </tbody>
              </table>
              <p>#{I18n.t("pages.accessibility.keyboard_shortcuts.browser_table.description", locale: :es)}</p>
              <table>
                <caption class='show-for-sr'>
                  #{I18n.t("pages.accessibility.keyboard_shortcuts.browser_table.caption", locale: :es)}
                </caption>
                <thead>
                  <tr>
                    <th scope='col'>
                      #{I18n.t("pages.accessibility.keyboard_shortcuts.browser_table.browser_header", locale: :es)}
                    </th>
                    <th scope='col'>
                      #{I18n.t("pages.accessibility.keyboard_shortcuts.browser_table.key_header", locale: :es)}
                    </th>
                  </tr>
                </thead>
                <tbody>"
  I18n.t("pages.accessibility.keyboard_shortcuts.browser_table.rows", locale: :es).each do |row|
    content << "  <tr>
                    <td>#{row[:browser_column]}</td>
                    <td>#{row[:key_column]}</td>
                  </tr>"
  end
  content << "  </tbody>
              </table>
              <h2>#{I18n.t("pages.accessibility.textsize.title", locale: :es)}</h2>
              <p>#{I18n.t("pages.accessibility.textsize.browser_settings_table.description", locale: :es)}</p>
              <table>
                <thead>
                  <tr>
                    <th scope='col'>
                      #{I18n.t("pages.accessibility.textsize.browser_settings_table.browser_header", locale: :es)}
                    </th>
                    <th scope='col'>
                      #{I18n.t("pages.accessibility.textsize.browser_settings_table.action_header", locale: :es)}
                    </th>
                  </tr>
                </thead>
                <tbody>"
  I18n.t("pages.accessibility.textsize.browser_settings_table.rows", locale: :es).each do |row|
    content << "  <tr>
                    <td>#{row[:browser_column]}</td>
                    <td>#{row[:action_column]}</td>
                  </tr>"
  end
  content << "  </tbody>
              </table>"
  content << "<p>#{I18n.t("pages.accessibility.textsize.browser_shortcuts_table.description", locale: :es)}</p>
              <ul>"
  I18n.t("pages.accessibility.textsize.browser_shortcuts_table.rows", locale: :es).each do |row|
    content << "<li><strong>#{row[:shortcut_column]}</strong> #{row[:description_column]}</li>"
  end
  content << "</ul>
              <h2>#{I18n.t("pages.accessibility.compatibility.title", locale: :es)}</h2>
              <p>#{I18n.t("pages.accessibility.compatibility.description", locale: :es)}</p>"

  page.content_es = content
  page.save!
end
