if SiteCustomization::Page.find_by_slug("welcome_not_verified").present?
  page = SiteCustomization::Page.find_by_slug("welcome_not_verified")
  page.title_es = I18n.t("welcome.welcome.title")

  page.content_es = "<p>#{I18n.t("welcome.welcome.user_permission_info", locale: :es)}</p>
                  <ul>
                    <li>#{I18n.t("welcome.welcome.user_permission_debates", locale: :es)}</li>
                    <li>#{I18n.t("welcome.welcome.user_permission_proposal", locale: :es)}</li>
                  </ul>

                  <p>#{I18n.t("welcome.welcome.user_permission_verify", locale: :es)}</p>
                  <ul>
                    <li>#{I18n.t("welcome.welcome.user_permission_support_proposal", locale: :es)}</li>
                    <li>#{I18n.t("welcome.welcome.user_permission_votes", locale: :es)}</li>
                  </ul>

                  <p>#{I18n.t("welcome.welcome.user_permission_verify_info", locale: :es)}</p>

                  <a href='/verification' class='button success radius expand'>
                    #{I18n.t("welcome.welcome.user_permission_verify_my_account", locale: :es)}
                  </a>

                  <p><a href='/'>#{I18n.t("welcome.welcome.go_to_index", locale: :es)}</a></p>"
  page.save!
end
