if SiteCustomization::Page.find_by_slug("census_terms").present?
  page = SiteCustomization::Page.find_by_slug("census_terms")
  page.title_es = I18n.t("pages.census_terms.title", locale: :es)
  page.content_es = "<p>#{I18n.t("pages.census_terms.description", locale: :es)}</p>"
  page.save!
end
