# coding: utf-8
require "sms_config"

# Default admin user (change password after first deploy to a server!)
if Administrator.count == 0 && !Rails.env.test?
  admin = User.create!(username: "admin", email: "admin@consul.dev", password: "12345678++",
                       password_confirmation: "12345678++", confirmed_at: Time.current,
                       terms_of_service: "1")
  admin.create_administrator
end

# Settings
Setting.reset_defaults

# Custom settings
Setting["uploads.images.content_types"] = "image/jpeg image/png"
Setting["uploads.images.max_size"] = "2"
Setting["feature.google_login"] = nil
Setting["feature.facebook_login"] = nil
Setting["feature.twitter_login"] = nil
Setting["feature.user.skip_verification"] = nil
Setting["valid_postal_codes"] = nil
Setting["mailer_from_name"] = Rails.application.secrets.mailer_from_name
Setting["mailer_from_address"] = Rails.application.secrets.mailer_from_address

WebSection.where(name: "homepage").first_or_create!
WebSection.where(name: "debates").first_or_create!
WebSection.where(name: "proposals").first_or_create!
WebSection.where(name: "budgets").first_or_create!
WebSection.where(name: "help_page").first_or_create!

# Default custom pages
load Rails.root.join("db", "pages.rb")

# Spanish translations
load Rails.root.join("db", "pages_es.rb")

# Footer links
footer_links_ca = SiteCustomization::ContentBlock.new
footer_links_ca.name = "footer"
footer_links_ca.locale = "ca"
footer_links_ca.body = <<-CONTENT
  <li><a href='http://www.conselldemallorca.net'>Consell de Mallorca</a></li>
  <li><a href='https://entretothom.net/accessibility'>Accessibilitat</a></li>
CONTENT
footer_links_ca.save

footer_links_es = SiteCustomization::ContentBlock.new
footer_links_es.name = "footer"
footer_links_es.locale = "ca"
footer_links_es.body = <<-CONTENT
  <li><a href='http://www.conselldemallorca.net'>Consell de Mallorca</a></li>
  <li><a href='https://entretothom.net/accessibility'>Accesibilidad</a></li>
CONTENT
footer_links_es.save

# Header links
header_links_ca = SiteCustomization::ContentBlock.new
header_links_ca.name = "top_links"
header_links_ca.locale = "es"
header_links_ca.body = <<-CONTENT
  <li>
    <a target="_blank" rel="nofollow" title="Ir a la página de Transparencia (se abre en ventana nueva)" href="https://conselldemallorca.transparencialocal.gob.es/">Transparencia</a>
  </li>
  <li>
    <a target="_blank" rel="nofollow" title="Ir a la página de Sede Electrónica (se abre en ventana nueva)" href="https://seu.conselldemallorca.net">Sede Electrónica</a>
  </li>
    <li>
      <a target="_blank" rel="nofollow" title="Ir a la página de Blog (se abre en ventana nueva)" href="http://participaciociutadanamallorca.cat/">Blog</a>
    </li>
  <li>
    <a href="/accessibility">Accesibilidad</a>
  </li>
CONTENT
header_links_ca.save

header_links_es = SiteCustomization::ContentBlock.new
header_links_es.name = "top_links"
header_links_es.locale = "ca"
header_links_es.body = <<-CONTENT
  <li>
    <a target="_blank" rel="nofollow" title="Anar a la pàgina de Transparència (s'obre en una finestra nova)" href="https://conselldemallorca.transparencialocal.gob.es/">Transparència</a>
  </li>
  <li>
    <a target="_blank" rel="nofollow" title="Anar a la pàgina de Seu Electrònica (s'obre en una finestra nova)" href="https://seu.conselldemallorca.net">Seu Electrònica</a>
  </li>
    <li>
      <a target="_blank" rel="nofollow" title="Anar a la pàgina de Blog (s'obre en una finestra nova)" href="http://participaciociutadanamallorca.cat/">Blog</a>
    </li>
  <li>
    <a href="/accessibility">Accessibilitat</a>
  </li>
CONTENT
header_links_es.save

# Home page - Header
Widget::Card.create!(
  title_ca: "<b>Entre tothom</b> decidim la <b>Mallorca</b> que volem",
  title_es: "<b>Entre todos</b> decidimos la <b>Mallorca</b> que queremos",

  description_ca: "",
  description_es: "",

  link_text_ca: "Registra't i participa",
  link_text_es: "Regístrate y participa",

  label_ca: "",
  label_es: "",

  link_url: "/users/sign_up",
  header: true,
  image_attributes: {
    attachment: File.open(Rails.root.join("app/assets/images/custom/welcome.png")),
    title: "welcome.png",
    user: User.first
  }
)

# SMS config
sms_config = SMSConfig.new
sms_config.update_sms_available(1000)
