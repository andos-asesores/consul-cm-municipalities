class AddUniqueConstraintToVotes < ActiveRecord::Migration[5.0]
  def change
    add_index :votes, [:voter_id, :votable_id, :voter_type, :vote_scope], :unique => true, name: "index_votes_unique_voter_and_votable_fields"
  end
end
