require_dependency Rails.root.join("app", "models", "image").to_s

class Image < ApplicationRecord
  include ImagesHelper
  include ImageablesHelper

  has_attached_file :attachment, styles: {
                                   large: "x#{Setting["uploads.images.min_height"]}",
                                   medium: "300x300#",
                                   thumb: "140x245#"
                                 },
                                 url: "/system/:class/:prefix/:style/:hash.:extension",
                                 # ANDOS UPDATE - Actualizada ruta donde se almacenan las imágenes
                                 path: "#{Rails.application.secrets.app_name}/system/:class/:prefix/:style/:hash.:extension",
                                 # END ANDOS UPDATE
                                 hash_data: ":class/:style",
                                 use_timestamp: false,
                                 hash_secret: Rails.application.secrets.secret_key_base

  def set_attachment_from_cached_attachment
    self.attachment = if Paperclip::Attachment.default_options[:storage] == :filesystem
                        File.open(cached_attachment)
                      else
                        # ANDOS UPDATE - Corregido error al guardar la imagen desde una URL
                        Paperclip.io_adapters.for(URI.parse(cached_attachment).to_s, { hash_digest: Digest::MD5 })
                        # END ANDOS UPDATE
                      end
  end
end
