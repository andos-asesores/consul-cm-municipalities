require_dependency Rails.root.join('app', 'models', 'verification', 'sms').to_s

class Verification::Sms
  include ActiveModel::Model

  validate :uniqness_phone

  def uniqness_phone
    alternative_phone = ( phone.starts_with?("+34") ) ? phone.gsub("+34", "") : "+34#{phone}"
    if User.where(confirmed_phone: [phone, alternative_phone]).any?
      errors.add(:phone, :taken)
    end
  end
end
