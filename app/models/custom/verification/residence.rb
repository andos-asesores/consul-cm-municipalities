require_dependency Rails.root.join("app", "models", "verification", "residence").to_s

class Verification::Residence
  validate :postal_code_in_municipality
  validate :residence_in_municipality

  def postal_code_in_municipality
    errors.add(:postal_code, I18n.t("verification.residence.new.error_not_allowed_postal_code")) unless valid_postal_code?
  end

  def residence_in_municipality
    return if errors.any?

    unless residency_valid?
      errors.add(:residence_in_municipality, false)
      store_failed_attempt
      Lock.increase_tries(user)
    end
  end

  private

    def valid_postal_code?
      return true if Setting["valid_postal_codes"].blank?

      valid_postal_codes = Setting["valid_postal_codes"].split(",").map(&:strip)
      valid_postal_codes.include?(postal_code)
    end
end
