require_dependency Rails.root.join("app", "models", "ckeditor", "picture").to_s

class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                    url: "/ckeditor_assets/pictures/:id/:style_:basename.:extension",
                    # ANDOS UPDATE - Actualizada ruta donde se almacenan las imágenes
                    path: "#{Rails.application.secrets.app_name}/ckeditor_assets/pictures/:id/:style_:basename.:extension",
                    # END ANDOS UPDATE
                    styles: { content: "800>", thumb: "118x100#" }
end
