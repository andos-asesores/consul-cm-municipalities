require_dependency Rails.root.join("app", "models", "setting").to_s

class Setting < ApplicationRecord
  def type
    if %w[feature process proposals map html homepage uploads read_only].include? prefix
      prefix
    elsif %w[remote_census].include? prefix
      key.rpartition(".").first
    else
      "configuration"
    end
  end
end
