require_dependency Rails.root.join("app", "models", "site_customization", "image").to_s

class SiteCustomization::Image < ApplicationRecord
  # ANDOS UPDATE - Actualizado tamaño permitido de 'logo_header'
  VALID_IMAGES = {
    "logo_header" => [456, 53],
    "social_media_icon" => [470, 246],
    "social_media_icon_twitter" => [246, 246],
    "apple-touch-icon-200" => [200, 200],
    "budget_execution_no_image" => [800, 600],
    "map" => [420, 500],
    "logo_email" => [400, 80]
  }.freeze

  has_attached_file :image,
                    path: "#{Rails.application.secrets.app_name}/system/:class/:id_partition/:style/:basename.:extension"
end
