require_dependency Rails.root.join("app", "models", "document").to_s

class Document < ApplicationRecord
  include DocumentsHelper
  include DocumentablesHelper
  has_attached_file :attachment, url: "/system/:class/:prefix/:style/:hash.:extension",
                                 # ANDOS UPDATE - Actualizada ruta donde se almacenan los documentos
                                 path: "#{Rails.application.secrets.app_name}/system/:class/:prefix/:style/:hash.:extension",
                                 # END ANDOS UPDATE
                                 hash_data: ":class/:style/:custom_hash_data",
                                 use_timestamp: false,
                                 hash_secret: Rails.application.secrets.secret_key_base

  def set_attachment_from_cached_attachment
    self.attachment = if Paperclip::Attachment.default_options[:storage] == :filesystem
                        File.open(cached_attachment)
                      else
                        # ANDOS UPDATE - Corregido error al guardar el documento desde una URL
                        Paperclip.io_adapters.for(URI.parse(cached_attachment).to_s, { hash_digest: Digest::MD5 })
                        # END ANDOS UPDATE
                      end
  end
end
