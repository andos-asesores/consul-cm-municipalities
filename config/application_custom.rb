module Consul
  class Application < Rails::Application
    config.i18n.enforce_available_locales = false
    config.i18n.default_locale = :ca
    config.i18n.available_locales = [:ca, :es]
    config.i18n.fallbacks = {"ca" => "es"}
  end
end
